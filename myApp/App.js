import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, ImageBackground } from 'react-native';

import Login from './login';
import Biodata from './Biodata';
import Hitung from './Hitungbalok';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor='#000'
          barstyle='light-content'
          />
          <Hitung />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
